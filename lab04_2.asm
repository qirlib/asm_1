global _main
extern _printf
extern _scanf

section .data
    welcome_str:
    db "Enter you FIO:", 10, 0
    output_str:
    db "**************", 10
    db "Your name is %s %s %s", 10
    db "**************", 10, 0

    buffer_o: times 1024 db 0
    buffer_i: times 1024 db 0
    buffer_f: times 1024 db 0
    scanf_str: db "%1023s %1023s %1023s", 0
    error_str: db "ERROR", 10, 0

section .text
_main:
    push welcome_str
    call _printf
    add esp, 4

    push buffer_o
    push buffer_i
    push buffer_f
    push scanf_str
    call _scanf
    add esp, 4 * 4
    
    cmp eax, 3
    jne error_exit

    push buffer_o
    push buffer_i
    push buffer_f
    push output_str
    call _printf
    add esp, 4 * 4

    xor eax, eax
    ret
error_exit:
    push error_str
    call _printf
    add esp, 4

    mov eax, 1
    ret
