global _main
extern _printf

section .data
    a: dd 0x0000FEDC
    b: dd 0x0000ABCD
    
    message:
    db "A: %p", 10
    db "B: %p", 10
    db "A + B: %p", 10
    db "A - B: %p", 10
    db "A AND B: %p", 10
    db "A OR B: %p", 10, 0

section .text
_main:
    mov esi, [a]
    mov edi, [b]

    mov eax, esi
    add eax, edi

    mov ebx, esi
    sub ebx, edi

    mov ecx, esi
    and ecx, edi

    mov edx, esi
    or edx, edi

    push edx
    push ecx
    push ebx
    push eax
    
    push edi
    push esi

    push message

    call _printf

    add esp, 4 * 7
    
    xor eax, eax
    ret
